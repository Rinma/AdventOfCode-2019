module.exports = function (wallaby) {
    return {
        files: [
            'src/**/*.js'
        ],

        tests: [
            'tests/**/*.spec.js'
        ],

        setup: function () {
            global.expect = require('chai').expect;
        },

        compilers: {
            '**/*.js': wallaby.compilers.babel()
        },

        env: {
            type: 'node',
            runner: 'node',
            params: {
                runner: `-r ${require.resolve('esm')}`
            }
        },
    };
};