import * as fs from "fs"
// ------------------------- DAY 01 ----------------------------------
import {day01} from './src/day01/day01.js'

const PART1 = false;
const PART2 = true;

let input01 = fs.readFileSync("input/day01.txt");
input01 = input01.toString().split("\n");

let d1_1 = new day01(input01).run(PART1);
let d1_2 = new day01(input01).run(PART2);

console.log(`Result for day 01 are 1/2: ${d1_1} and 2/2: ${d1_2}`);