export class day01 {
    constructor(input) {
        this.input = input
    }

    run(part2 = false) {
        let result = 0;

        for (let i of this.input) {
            i = parseInt(i);
            if (part2) {
                result += this.calculateFuelForModules(i, true)
            } else {
                result += this.calculateFuelForModules(i);
            }
        }

        return result;
    }

    calculateFuelForModules(input, calculateFuelFuel = false) {
        let r = this.getCalculation(input);

        if (calculateFuelFuel) {
            if (this.getCalculation(r) > 0) {
                r += this.calculateFuelForModules(r, true);
            }

            return r;
        } else {
            return r;
        }
    }

    getCalculation(i) {
        return Math.floor(parseInt(i) / 3) - 2;
    }
}