import {day01} from '../../src/day01/day01'

describe("Day 01 solution", () => {
    describe("Part 01", () => {
        it("when input 12 results in 2", () => {
            let d = new day01([12]);
            let r = d.run();

            expect(r).to.equal(2);
        });

        it("when input 14 results in 2", () => {
            let d = new day01([14]);
            let r = d.run();

            expect(r).to.equal(2);
        });

        it("when input 1969 results in 654", () => {
            let d = new day01([1969]);
            let r = d.run();

            expect(r).to.equal(654);
        });

        it("when input 100756 results in 33583", () => {
            let d = new day01([100756]);
            let r = d.run();

            expect(r).to.equal(33583);
        });

        it("when input 100756 & 1736 results in 34162", () => {
            let d = new day01([100756, 1736]);
            let r = d.run();

            expect(r).to.equal(34159);
        });
    });

    describe("Part 02", () => {
        it("when input 14 results in 2", () => {
            let d = new day01([14]);
            let r = d.run(true);

            expect(r).to.equal(2);
        });

        it("when input 1969 results in 966", () => {
            let d = new day01([1969]);
            let r = d.run(true);

            expect(r).to.equal(966);
        });

        it("when input 100756 results in 50346", () => {
            let d = new day01([100756]);
            let r = d.run(true);

            expect(r).to.equal(50346);
        });
    });
});